<?php

require __DIR__ . '/vendor/autoload.php';

use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

$app = new \Slim\App(
    new \Slim\Container([
        'settings' => [
            'displayErrorDetails' => true,
            'fb' => [
                'access_token' => getenv('FB_PAGE_TOKEN'),
                'endpoint' => 'https://graph.facebook.com/v2.6/me/messages'
            ]
        ]
    ])
);

// container
$container = $app->getContainer();

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('basic_logger');
    $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
    return $logger;
};

$container['client'] = function($c) {
    $client = new  GuzzleHttp\Client([
        'base_uri' => $c['settings']['fb']['endpoint'],
    ]);
    return $client;
};

$container['view'] = function($c) {
    $view = new \Slim\Views\Twig(
        realpath(__DIR__ . '/app/templates'),
        [
            'cache' => '/tmp/cache',
            'debug' => true
        ]
    );
    $basePath = str_ireplace('index.php', '', $c['request']->getUri()->getBasePath());
    $basePath = rtrim($basePath, '/');
    $view->addExtension(
        new \Slim\Views\TwigExtension($c['router'], $basePath)
    );

    $twig = $view->getEnvironment();

    // twig filter
    $twig->addFilter(
        new \Twig_SimpleFilter('json_decode', function ($string) {
            return json_decode($string);
        })
    );

    $view['script'] = 'script';
    return $view;
};

// index page
$app->get('/', function($req, $res) use ($container) {
    $container->view->render(
        $res->withStatus(200),
        'index.twig',
        [
            'fb_app_id' => getenv('FB_APP_ID'),
            'fb_page_id' => getenv('FB_PAGE_ID'),
        ]
    );
})->setName('index');


// hub.charenge
$app->get('/webhook', function($req, $res) use ($container) {
    $token = getenv('FB_VERIFY_TOKEN');
    $verifyToken = $req->getParam('hub_verify_token');
    $challenge = $req->getParam('hub_challenge');

    if ($token === $verifyToken) {
        return $res->write($challenge);
    }

    $container->logger->addInfo($challenge);
    return $res->withStatus(400)
        ->write('Error');
});

$app->post('/webhook', function($req, $res) use ($container) {
    $token = $container->settings['fb']['access_token'];
    $body = json_decode($req->getBody(), true);
    $logger = $container->logger;
    $logger->addInfo(json_encode($body));

    foreach ($body['entry'] as $entry) {
        $logger->addInfo(json_encode($entry));

        $user = $entry['messaging'][0]['sender']['id'];
        $logger->addInfo($user);

        // 自分が送信した時もコールバックが来るので判定
        // isDevivered
        if (array_key_exists('delivery', $entry['messaging'][0])) {
            // なにもしない
            // うっかり返信すると無限ループになりかねないので注意
            $logger->addInfo('DELIVERY');
            continue;
        }

        $messageText = 'いいねください';

        // postbackに返信
        if (isset($entry['messaging'][0]['postback'])) {
            $logger->addInfo('POSTBACK');
            $messageText = 'Postbackいただきました！';
        }

        // optinsに返信
        if ($entry['messaging'][0]['optin']) {
            $logger->addInfo('OPTINS: ' . $entry['messaging'][0]['optin']['ref']);
            $messageText = 'Send to Messenger押した?';
        }

        // 返信業務
        $reply = [
            'recipient' => [
                'id' => $user
            ],
            'message' => [
                'text' => $messageText
            ] 
        ];

        $client = $container['client'];
        $response = $client->request('POST', '', [
            'query' => [
                'access_token' => $token,
            ],
            'json' => $reply
        ]);
    } 
});

$app->run();
